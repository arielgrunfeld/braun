{-
    Queue with amortized constant cost
-}
module Queue
    ( Queue
    , empty
    , enq
    , deq
    ) where

type Queue a = ([a], [a])

empty :: Queue a
empty = ([],[])

enq :: a -> Queue a -> Queue a
enq x (inbox, outbox) = (x:inbox, outbox)

deq :: Queue a -> Maybe (a, Queue a)
deq ([]   , []      ) = Nothing
deq (inbox, x:outbox) = Just (x, (inbox, outbox))
deq (inbox, []      ) = deq ([], reverse inbox)
