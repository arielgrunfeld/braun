# Braun Trees

Chris Okasaki's paper *Three algorithms on Braun trees*<sup>1</sup> ends with an exercise to invert the linear conversion from lists to Braun trees.

This is an attempt to solve this exercise.

## Sources
1. [Okasaki, Chris. "**Three algorithms on Braun trees**" *Journal of Functional Programming*  7, no. 6 (1997): 661-666. (Cambridge University Press)](https://www.cambridge.org/core/journals/journal-of-functional-programming/article/three-algorithms-on-braun-trees/59CF6B17F75EC216EBCA8A08E8780DFF)
