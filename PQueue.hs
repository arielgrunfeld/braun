{-# LANGUAGE ViewPatterns #-}
{-
    Bounded Priority Queue with 4 priorities.
    CurrentLeft > CurrentRight > NextLeft > NextRight
-}
module PQueue
    ( PQueue
    , empty
    , enq
    , enqPair
    , deq
    ) where

import           Queue (Queue)
import qualified Queue as Q

-- (CurrentLeft, CurrentRight, NextLeft, NextRight)
type PQueue a = (Queue a, Queue a, Queue a, Queue a)

empty :: PQueue a
empty = (Q.empty, Q.empty, Q.empty, Q.empty)

enq :: Either a a -> PQueue a -> PQueue a
enq (Left x)  (cl, cr, nl, nr) = (cl, cr, Q.enq x nl, nr)
enq (Right x) (cl, cr, nl, nr) = (cl, cr, nl, Q.enq x nr)

enqPair :: a -> a -> PQueue a -> PQueue a
enqPair l r (cl, cr, nl, nr) = (cl, cr, Q.enq l nl, Q.enq r nr)

-- If the current queues are empty we move the next queues to replace them.
deq :: PQueue a -> Maybe (a, PQueue a)
deq (Q.deq->Nothing, Q.deq->Nothing, Q.deq->Nothing, Q.deq->Nothing) =
    Nothing
deq (Q.deq->Nothing, Q.deq->Nothing, Q.deq->Nothing, Q.deq->Just(x,nr)) =
    Just (x, (Q.empty, nr, Q.empty, Q.empty))
deq (Q.deq->Nothing, Q.deq->Nothing, Q.deq->Just(x,nl), nr) =
    Just (x, (nl, nr, Q.empty, Q.empty))
deq (Q.deq->Nothing, Q.deq->Just(x,cr), nl, nr) =
    Just (x, (Q.empty, cr, nl, nr))
deq (Q.deq->Just(x,cl), cr, nl, nr) =
    Just (x, (cl, cr, nl, nr))
deq _ = error "impossible pattern"
