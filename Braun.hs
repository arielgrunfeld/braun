module Braun where

import Data.Bifunctor (bimap)
import PQueue (PQueue, empty, deq, enqPair)

data Tree a
    = Leaf
    | Node !a !(Tree a) !(Tree a)
    deriving (Show, Eq, Ord)


---------- Chris Okasaki's linear List to Braun conversion --------------------

rows :: Int -> [a] -> [(Int, [a])]
rows _ [] = []
rows k xs =
    let (pre, post) = splitAt k xs
    in (k, pre) : rows (2 * k) post

build :: (Int, [a]) -> [Tree a] -> [Tree a]
build (k, xs) ts = zipWith3 Node xs ts1 ts2
    where (ts1, ts2) = splitAt k (ts ++ repeat Leaf)

makeArray :: [a] -> Tree a
makeArray = head . foldr build [Leaf] . rows 1


---------- Linear Braun to List conversion ------------------------------------

type Env a = ([a], PQueue (Tree a))

emptyEnv :: Env a
emptyEnv = ([], empty)

updateEnv :: Tree a -> Env a -> Env a
updateEnv Leaf         = id
updateEnv (Node x l r) = bimap (x:) (enqPair l r)

unbuild :: Env a -> Tree a -> [a]
unbuild env t =
    let (xs,ts) = updateEnv t env
    in case deq ts of
        Nothing       -> xs
        Just (t',ts') -> unbuild (xs,ts') t'

unmakeArray :: Tree a -> [a]
unmakeArray = reverse . unbuild emptyEnv
